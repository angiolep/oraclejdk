# oraclejdk
Docker images for GitLab Runner providing Oracle JDK - Java Development toolKit

```bash
docker login \
  registry.gitlab.com

docker image build \
  --pull \
  --tag registry.gitlab.com/angiolep/oraclejdk:1.8.0_171 \
  .

docker image push \
  registry.gitlab.com/angiolep/oraclejdk:1.8.0_171
```
